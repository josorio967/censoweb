/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.censoweb.view;

import co.edu.sena.censoweb.business.SuscriptorBeanLocal;
import co.edu.sena.censoweb.model.Suscriptor;
import co.edu.sena.censoweb.utils.MessageUtils;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aprendiz
 */
public class SuscriptorView {
    private InputText txtDocumento;
    private SelectOneMenu selectTipoDocumento;
    private InputText txtPrimerNombre;
    private InputText txtSegundoNombre;
    private InputText txtPrimerApellido;
    private InputText txtSegundoApellido;
    private InputText txtTelefono;
    private InputText txtEmail;
    private CommandButton btnCrear;
    private CommandButton btnModificar;
    private CommandButton btnEliminar;
    private List<Suscriptor> listSuscriptores = null;
    
    @EJB
    private SuscriptorBeanLocal suscriptorBean;
    
    
    /**
     * Creates a new instance of SuscriptorView
     */
    public SuscriptorView() {
    }

    public InputText getTxtDocumento() {
        return txtDocumento;
    }

    public void setTxtDocumento(InputText txtDocumento) {
        this.txtDocumento = txtDocumento;
    }

    public SelectOneMenu getSelectTipoDocumento() {
        return selectTipoDocumento;
    }

    public void setSelectTipoDocumento(SelectOneMenu selectTipoDocumento) {
        this.selectTipoDocumento = selectTipoDocumento;
    }    

    public InputText getTxtPrimerNombre() {
        return txtPrimerNombre;
    }

    public void setTxtPrimerNombre(InputText txtPrimerNombre) {
        this.txtPrimerNombre = txtPrimerNombre;
    }

    public InputText getTxtSegundoNombre() {
        return txtSegundoNombre;
    }

    public void setTxtSegundoNombre(InputText txtSegundoNombre) {
        this.txtSegundoNombre = txtSegundoNombre;
    }

    public InputText getTxtPrimerApellido() {
        return txtPrimerApellido;
    }

    public void setTxtPrimerApellido(InputText txtPrimerApellido) {
        this.txtPrimerApellido = txtPrimerApellido;
    }

    public InputText getTxtSegundoApellido() {
        return txtSegundoApellido;
    }

    public void setTxtSegundoApellido(InputText txtSegundoApellido) {
        this.txtSegundoApellido = txtSegundoApellido;
    }

    public InputText getTxtTelefono() {
        return txtTelefono;
    }

    public void setTxtTelefono(InputText txtTelefono) {
        this.txtTelefono = txtTelefono;
    }

    public InputText getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(InputText txtEmail) {
        this.txtEmail = txtEmail;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<Suscriptor> getListSuscriptores() {
        try {
            if(listSuscriptores == null)
            {
                listSuscriptores = suscriptorBean.findAll();
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
        
        return listSuscriptores;
    }

    public void setListSuscriptores(List<Suscriptor> listSuscriptores) {
        this.listSuscriptores = listSuscriptores;
    }
    
    public void clear()
    {
        txtDocumento.setValue("");
        selectTipoDocumento.setValue("");
        txtEmail.setValue("");
        txtTelefono.setValue("");
        txtPrimerApellido.setValue("");
        txtPrimerNombre.setValue("");
        txtSegundoApellido.setValue("");
        txtSegundoNombre.setValue("");
        btnCrear.setDisabled(false);
        btnEliminar.setDisabled(true);
        btnModificar.setDisabled(true);
        listSuscriptores = null;
    }
    
    public void insert()
    {
        try {
            Suscriptor suscriptor = new Suscriptor();
            suscriptor.setDocumento(Long.parseLong(txtDocumento.getValue().toString()));
            suscriptor.setTipoDocumento(selectTipoDocumento.getValue().toString());
            suscriptor.setPrimerNombre(txtPrimerNombre.getValue().toString());
            suscriptor.setSegundoNombre(txtSegundoNombre.getValue().toString());
            suscriptor.setPrimerApellido(txtPrimerApellido.getValue().toString());
            suscriptor.setSegundoApellido(txtSegundoApellido.getValue().toString());
            suscriptor.setTelefono(txtTelefono.getValue().toString());
            suscriptor.setEmail(txtEmail.getValue().toString());
            
            suscriptorBean.insert(suscriptor);
            clear();
            MessageUtils.addInfoMessage("Suscriptor creado exitosamente");            
        } catch(NumberFormatException e){
            MessageUtils.addErrorMessage("El documento debe ser un número");
        }       
        catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }
    
    public void update()
    {
        try {
            Suscriptor suscriptor = new Suscriptor();
            suscriptor.setDocumento(Long.parseLong(txtDocumento.getValue().toString()));
            suscriptor.setTipoDocumento(selectTipoDocumento.getValue().toString());
            suscriptor.setPrimerNombre(txtPrimerNombre.getValue().toString());
            suscriptor.setSegundoNombre(txtSegundoNombre.getValue().toString());
            suscriptor.setPrimerApellido(txtPrimerApellido.getValue().toString());
            suscriptor.setSegundoApellido(txtSegundoApellido.getValue().toString());
            suscriptor.setTelefono(txtTelefono.getValue().toString());
            suscriptor.setEmail(txtEmail.getValue().toString());
            
            suscriptorBean.update(suscriptor);
            clear();
            MessageUtils.addInfoMessage("Suscriptor modificado exitosamente");            
        } catch(NumberFormatException e){
            MessageUtils.addErrorMessage("El documento debe ser un número");
        }       
        catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }
    
    public void delete()
    {
        try {
            Suscriptor suscriptor = new Suscriptor();
            suscriptor.setDocumento(Long.parseLong(txtDocumento.getValue().toString()));            
            
            suscriptorBean.delete(suscriptor);
            clear();
            MessageUtils.addInfoMessage("Suscriptor eliminado exitosamente");            
        } catch(NumberFormatException e){
            MessageUtils.addErrorMessage("El documento debe ser un número");
        }       
        catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }
    
    public void rowSelect(SelectEvent event)
    {
        Suscriptor suscriptor = (Suscriptor) event.getObject();
        txtDocumento.setValue(suscriptor.getDocumento());
        selectTipoDocumento.setValue(suscriptor.getTipoDocumento());
        txtEmail.setValue(suscriptor.getEmail());
        txtTelefono.setValue(suscriptor.getTelefono());
        txtPrimerApellido.setValue(suscriptor.getPrimerApellido());
        txtPrimerNombre.setValue(suscriptor.getPrimerNombre());
        txtSegundoApellido.setValue(suscriptor.getSegundoApellido());
        txtSegundoNombre.setValue(suscriptor.getSegundoNombre());
        btnCrear.setDisabled(true);
        btnEliminar.setDisabled(false);
        btnModificar.setDisabled(false);
    }
    
}
