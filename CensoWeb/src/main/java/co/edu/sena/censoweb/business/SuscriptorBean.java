/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Suscriptor;
import co.edu.sena.censoweb.persistence.ISuscriptorDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author anfel
 */
@Stateless
public class SuscriptorBean implements SuscriptorBeanLocal {

    @EJB
    private ISuscriptorDAO suscriptorDAO;
    
    public void validate(Suscriptor suscriptor) throws Exception
    {
        if(suscriptor == null)
        {
            throw new Exception("El suscriptor es nulo");
        }
        
        if(suscriptor.getDocumento() == 0)
        {
            throw new Exception("El documento es obligatorio");
        }
        
        if(suscriptor.getTipoDocumento().isEmpty())
        {
            throw new Exception("El tipo de documento es obligatorio");
        }
        
        if(suscriptor.getPrimerNombre().isEmpty())
        {
            throw new Exception("El primer nombre es obligatorio");
        }
        
        if(suscriptor.getPrimerApellido().isEmpty())
        {
            throw new Exception("El primer apellido es obligatorio");
        }
    }

    @Override
    public void insert(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if(oldSuscriptor != null)
        {
            throw new Exception("Ya existe un suscriptor con el mismo documento");
        }
        
        suscriptorDAO.insert(suscriptor);
    }

    @Override
    public void update(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if(oldSuscriptor == null)
        {
            throw new Exception("No existe un suscriptor con ese documento");
        }
        
        oldSuscriptor.setTipoDocumento(suscriptor.getTipoDocumento());
        oldSuscriptor.setEmail(suscriptor.getEmail());
        oldSuscriptor.setPrimerApellido(suscriptor.getPrimerApellido());
        oldSuscriptor.setPrimerNombre(suscriptor.getPrimerNombre());
        oldSuscriptor.setSegundoApellido(suscriptor.getSegundoApellido());
        oldSuscriptor.setSegundoNombre(suscriptor.getSegundoNombre());
        oldSuscriptor.setTelefono(suscriptor.getTelefono());
        suscriptorDAO.update(oldSuscriptor);
    }

    @Override
    public void delete(Suscriptor suscriptor) throws Exception {
        if(suscriptor.getDocumento() == 0)
        {
            throw new Exception("El documento es obligatorio");
        }
        
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if(oldSuscriptor == null)
        {
            throw new Exception("No existe un suscriptor con ese documento");
        }
        
        suscriptorDAO.delete(oldSuscriptor);
    }

    @Override
    public Suscriptor findById(Long documento) throws Exception {
        if(documento == 0)
        {
            throw new Exception("El documento es obligatorio");
        }
        
        return suscriptorDAO.findById(documento);
    }

    @Override
    public List<Suscriptor> findAll() throws Exception {
        return suscriptorDAO.findAll();
    }
    
    
}
