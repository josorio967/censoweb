/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.censoweb.view;

import co.edu.sena.censoweb.business.ServicioBeanLocal;
import co.edu.sena.censoweb.model.Servicio;
import co.edu.sena.censoweb.utils.MessageUtils;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aprendiz
 */
public class ServicioView {

    private InputText txtId;
    private InputText txtSerieMedidor;
    private InputText txtLectura;
    private SelectOneMenu selectMarcaMedidor;
    private SelectBooleanCheckbox checkAlcantarillado;
    private SelectBooleanCheckbox checkAcueducto;
    private List<Servicio> listServicios = null;
    private CommandButton btnCrear;
    private CommandButton btnModificar;
    private CommandButton btnEliminar;

    @EJB
    private ServicioBeanLocal servicioBean;

    /**
     * Creates a new instance of ServicioView
     */
    public ServicioView() {
    }

    public InputText getTxtId() {
        return txtId;
    }

    public void setTxtId(InputText txtId) {
        this.txtId = txtId;
    }

    public InputText getTxtSerieMedidor() {
        return txtSerieMedidor;
    }

    public void setTxtSerieMedidor(InputText txtSerieMedidor) {
        this.txtSerieMedidor = txtSerieMedidor;
    }

    public InputText getTxtLectura() {
        return txtLectura;
    }

    public void setTxtLectura(InputText txtLectura) {
        this.txtLectura = txtLectura;
    }

    public SelectOneMenu getSelectMarcaMedidor() {
        return selectMarcaMedidor;
    }

    public void setSelectMarcaMedidor(SelectOneMenu selectMarcaMedidor) {
        this.selectMarcaMedidor = selectMarcaMedidor;
    }

    public SelectBooleanCheckbox getCheckAlcantarillado() {
        return checkAlcantarillado;
    }

    public void setCheckAlcantarillado(SelectBooleanCheckbox checkAlcantarillado) {
        this.checkAlcantarillado = checkAlcantarillado;
    }

    public SelectBooleanCheckbox getCheckAcueducto() {
        return checkAcueducto;
    }

    public void setCheckAcueducto(SelectBooleanCheckbox checkAcueducto) {
        this.checkAcueducto = checkAcueducto;
    }

    public List<Servicio> getListServicios() {
        try {
            if (listServicios == null) {
                listServicios = servicioBean.findAll();
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }

        return listServicios;
    }

    public void setListServicios(List<Servicio> listServicios) {
        this.listServicios = listServicios;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public String getCheckValue(SelectBooleanCheckbox check) {
        if (check.isSelected()) {
            return "SI";
        } else {
            return "NO";
        }
    }

    public void clear() {
        txtId.setValue("");
        txtLectura.setValue("");
        txtSerieMedidor.setValue("");
        selectMarcaMedidor.setValue("");
        checkAcueducto.setSelected(false);
        checkAlcantarillado.setSelected(false);
        listServicios = null;
        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);
    }

    public void insert() {
        try {
            Servicio servicio = new Servicio();
            servicio.setAcueducto(getCheckValue(checkAcueducto));
            servicio.setAlcantarillado(getCheckValue(checkAlcantarillado));
            servicio.setLectura(new BigDecimal(txtLectura.getValue().toString()));
            servicio.setSerieMedidor(txtSerieMedidor.getValue().toString());
            servicio.setMarcaMedidor(selectMarcaMedidor.getValue().toString());
            servicioBean.insert(servicio);
            MessageUtils.addInfoMessage("Servicio creado exitosamente");
            clear();
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void update() {
        try {
            Servicio servicio = new Servicio();
            servicio.setIdServicio(Integer.parseInt(txtId.getValue().toString()));
            servicio.setAcueducto(getCheckValue(checkAcueducto));
            servicio.setAlcantarillado(getCheckValue(checkAlcantarillado));
            servicio.setLectura(new BigDecimal(txtLectura.getValue().toString()));
            servicio.setSerieMedidor(txtSerieMedidor.getValue().toString());
            servicio.setMarcaMedidor(selectMarcaMedidor.getValue().toString());
            servicioBean.update(servicio);
            MessageUtils.addInfoMessage("Servicio modificado exitosamente");
            clear();
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void delete() {
        try {
            Servicio servicio = new Servicio();
            servicio.setIdServicio(Integer.parseInt(txtId.getValue().toString()));
            servicioBean.delete(servicio);
            MessageUtils.addInfoMessage("Servicio eliminado exitosamente");
            clear();
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void rowSelect(SelectEvent event) {
        Servicio servicio = (Servicio) event.getObject();
        txtId.setValue(servicio.getIdServicio());
        txtLectura.setValue(servicio.getLectura());
        txtSerieMedidor.setValue(servicio.getSerieMedidor());
        selectMarcaMedidor.setValue(servicio.getMarcaMedidor());
        checkAcueducto.setSelected("SI".equals(servicio.getAcueducto()));
        checkAlcantarillado.setSelected("SI".equals(servicio.getAlcantarillado()));

        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

}
