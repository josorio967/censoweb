/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Predio;
import co.edu.sena.censoweb.persistence.IPredioDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class PredioBean implements PredioBeanLocal {

    @EJB
    private IPredioDAO predioDAO;
    
    private void validate(Predio predio) throws Exception
    {
        if(predio == null)
        {
            throw new Exception("El predio es nulo");
        }
        
        //si la PK es autoincremental, no se valida en este método
        
        if(predio.getAbreviatura().isEmpty())
        {
            throw new Exception("La abreviatura es obligatoria");
        }
        
        if(predio.getPrimerNumero() == 0)
        {
            throw new Exception("El primer número es obligatorio");
        }
        
        if(predio.getSegundoNumero() == 0)
        {
            throw new Exception("El segundo número es obligatorio");
        }
        
        if(predio.getTercerNumero() == 0)
        {
            throw new Exception("El tercer número es obligatorio");
        }
        
        if(predio.getBarrio().isEmpty())
        {
            throw new Exception("El barrio es obligatorio");
        }
        
        //validar las FK
        if(predio.getIdUso() == null)
        {
            throw new Exception("El uso comercial es obligatorio");
        }
    }

    @Override
    public void insert(Predio predio) throws Exception {
        validate(predio);
        //no se valida existencia del predio por id, porque este es autoincremental
        predioDAO.insert(predio);
    }

    @Override
    public void update(Predio predio) throws Exception {
        validate(predio);
        if(predio.getIdPredio() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Predio oldPredio = predioDAO.findById(predio.getIdPredio());
        if(oldPredio == null)
        {
            throw new Exception("No existe un predio con ese Id");
        }
        
        oldPredio.setAbreviatura(predio.getAbreviatura());
        oldPredio.setBarrio(predio.getBarrio());
        oldPredio.setComplemento(predio.getComplemento());
        oldPredio.setIdUso(predio.getIdUso());
        oldPredio.setNumeroOcupantes(predio.getNumeroOcupantes());
        oldPredio.setNumeroPisos(predio.getNumeroPisos());
        oldPredio.setPrimerNumero(predio.getPrimerNumero());
        oldPredio.setSegundoNumero(predio.getSegundoNumero());
        oldPredio.setTercerNumero(predio.getTercerNumero());
        predioDAO.update(oldPredio);
    }

    @Override
    public void delete(Predio predio) throws Exception {
        if(predio.getIdPredio() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Predio oldPredio = predioDAO.findById(predio.getIdPredio());
        if(oldPredio == null)
        {
            throw new Exception("No existe un predio con ese Id");
        }
        
        predioDAO.delete(oldPredio);
    }

    @Override
    public Predio findById(Integer idPredio) throws Exception {
        if(idPredio == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        return predioDAO.findById(idPredio);
    }

    @Override
    public List<Predio> findAll() throws Exception {
        return predioDAO.findAll();
    }
    
    
}
