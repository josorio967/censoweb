/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Servicio;
import co.edu.sena.censoweb.persistence.IServicioDAO;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ServicioBean implements ServicioBeanLocal {

    @EJB
    private IServicioDAO servicioDAO;
    
    private void validate(Servicio servicio) throws Exception
    {
        if(servicio == null)
        {
            throw new Exception("El servicio es nulo");
        }
        
        if(servicio.getAcueducto().isEmpty())
        {
            throw new Exception("El acueducto es obligatorio");
        }
        
        if(servicio.getAlcantarillado().isEmpty())
        {
            throw new Exception("El alcantarillado es obligatorio");
        }
        
        if(servicio.getLectura() == BigDecimal.ZERO)
        {
            throw new Exception("La lectura es obligatoria");
        }
        
        if(servicio.getMarcaMedidor().isEmpty())
        {
            throw new Exception("La marca del medidor es obligatoria");
        }
        
        if(servicio.getSerieMedidor().isEmpty())
        {
            throw new Exception("La serie del medidor es obligatoria");
        }        
    }

    @Override
    public void insert(Servicio servicio) throws Exception {
        validate(servicio);
        servicioDAO.insert(servicio);
    }

    @Override
    public void update(Servicio servicio) throws Exception {
        validate(servicio);
        
        if(servicio.getIdServicio() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Servicio oldServicio= servicioDAO.findById(servicio.getIdServicio());
        if(oldServicio == null)
        {
            throw new Exception("No existe un predio con ese Id");
        }
        
        oldServicio.setAcueducto(servicio.getAcueducto());
        oldServicio.setAlcantarillado(servicio.getAlcantarillado());
        oldServicio.setLectura(servicio.getLectura());
        oldServicio.setMarcaMedidor(servicio.getMarcaMedidor());
        oldServicio.setSerieMedidor(servicio.getSerieMedidor());
        servicioDAO.update(oldServicio);
    }

    @Override
    public void delete(Servicio servicio) throws Exception {
        validate(servicio);
        
        if(servicio.getIdServicio() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Servicio oldServicio= servicioDAO.findById(servicio.getIdServicio());
        if(oldServicio == null)
        {
            throw new Exception("No existe un predio con ese Id");
        }
        
        servicioDAO.delete(oldServicio);
    }

    @Override
    public Servicio findById(Integer idServicio) throws Exception {
        if(idServicio == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        return servicioDAO.findById(idServicio);
    }

    @Override
    public List<Servicio> findAll() throws Exception {
        return servicioDAO.findAll();
    }
    
    
}
