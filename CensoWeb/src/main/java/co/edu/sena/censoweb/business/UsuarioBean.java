/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Usuario;
import co.edu.sena.censoweb.persistence.IUsuarioDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author anfel
 */
@Stateless
public class UsuarioBean implements UsuarioBeanLocal {

    @EJB
    private IUsuarioDAO usuarioDAO;
    
    @Override
    public Usuario findById(String nombre) throws Exception {
        if(nombre.isEmpty())
        {
            throw new Exception("El nombre es obligatorio");
        }
        
        return usuarioDAO.findById(nombre);
    }

    @Override
    public List<Usuario> findAll() throws Exception {
        return usuarioDAO.findAll();
    }
    
    /**
     * 
     * @param password
     * @return  password
     */
    
    public String encryptPassword (String password){
        String encryptMD5 = DigestUtils.md5Hex(password);
        return encryptMD5;
 
    }

    @Override
    public void login(Usuario usuario) throws Exception {
     if(usuario ==null){
         throw  new Exception("el usuaario es nulo");
     }
     
     if(usuario.getNombre().isEmpty()){
         throw  new Exception("el usuario es obligatorio");
     }
     
     
     /*buscar un usuaario por el nombre */
     
     Usuario oldUsuario =usuarioDAO.findById(usuario.getNombre());
     
     if(oldUsuario ==null){
         throw  new Exception("el usuario es incorrrecto");
     }
     
     //contraseña incriptada en la base de datos 
     
     String passwordEncrypted = encryptPassword(usuario.getContrasena());
     
     
        if (oldUsuario.getContrasena().equals(passwordEncrypted)) {
              throw  new Exception("usuario  incorrrecto");
        }
     
    }

    
}
